﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using FormTestRazorMvc.Models;

namespace FormTestRazorMvc.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    [Consumes("application/x-www-form-urlencoded")]
    public IActionResult Index([FromForm] ShopResult item)
    {
        Console.WriteLine(item.Id);
        return RedirectToAction("Index", "Home");
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}